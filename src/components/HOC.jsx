import React from 'react';
import { Login } from './Login'
import { connect } from "react-redux";
import { Route } from 'react-router-dom';

function HOC({ component: Component, auth, ...rest }) {
    return <Route
        {...rest}
        render = {props => auth.isAuth ? console.log(props) : <Login />}
    />
};

const mapStateToProps = (state) => ({
    auth: state.auth
});

export default connect(
    mapStateToProps
)(HOC);
