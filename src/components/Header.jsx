import Navbar from "./Navbar";
import React, { useState } from "react";
import { connect } from "react-redux";
import { login, logout } from "../actionCreators/auth.action";

function Header(props) {
    const [statusValue, setStatusValue] = useState('Login');

    const authFunction = () => {
        console.log(props);
        if (props.auth.isAuth) {
            props.logout();
            setStatusValue("Login");
        } else {
            props.login();
            setStatusValue("Logout");
        }
    }

    return (
        <>
            <Navbar />
            <button onClick={authFunction}>{statusValue}</button>
        </>
    )
}

const mapStateToProps = (state) => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch) => ({
    login: (data) => { dispatch(login(data)) },
    logout: (data) => { dispatch(logout(data)) }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);
