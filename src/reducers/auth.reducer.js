import { auth as defaultState } from "../defaultStates/auth";
import { LOGIN, LOGOUT } from '../constants/auth.constants'

export default function ReducerAuthFunction (state = defaultState, action) {
    switch (action.type) {
        case LOGIN: 
            return state = {isAuth: true}; 
        case LOGOUT: 
            return state = {isAuth: false}; 
        default:
            return state;
    }
};