import { EDIT_USER, REQUEST_USER, REQUEST_USER_SUCCESS, REQUEST_USER_FAIL, FETCH_USER } from "../constants/profile.constants";

export const editUser = (data) => ({ type: EDIT_USER, data });
export const requestUser = () => ({ type: REQUEST_USER });
export const requestUserSuccess = (data) => ({ type: REQUEST_USER_SUCCESS, url: data });
export const requestUserError = () => ({ type: REQUEST_USER_FAIL });
export const fetchUser = () => ({ type: FETCH_USER });