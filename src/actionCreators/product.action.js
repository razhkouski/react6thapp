import { ADD_PRODUCT, DELETE_PRODUCT, EDIT_PRODUCT, FIND_PRODUCT, RESET_PRODUCTS } from "../constants/product.constants";

export const addProduct = (data) => ({type: ADD_PRODUCT, data});
export const editProduct = (data) => ({type: EDIT_PRODUCT, data});
export const deleteProduct = (id) => ({type: DELETE_PRODUCT, id});
export const findProduct = (data) => ({type: FIND_PRODUCT, data});
export const resetProducts = () => ({type: RESET_PRODUCTS});