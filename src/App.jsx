import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { store } from './store/store';
import { Provider } from 'react-redux';
import Header from './components/Header';
import HOC from "./components/HOC";
import Logo from './components/Logo';
import AdminPart from './components/AdminPart';
import UserPart from './components/UserPart';
import Profile from './components/Profile';

function App() {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Header />
                <Route path='/home' component={Logo} />
                <Route path='/adminPart' component={AdminPart} />
                <Route path='/userPart' component={UserPart} />
                <HOC exact path="/profile" component={Profile} />
            </BrowserRouter>
        </Provider>
    )
}

export default App;